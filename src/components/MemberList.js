import React, { useState, useEffect } from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import {
	Card
} from 'react-bulma-components';
import MemberRow from './MemberRow';

const MemberList = (props) => {
	const [members, setMembers] = useState([]);
	/*
		useState() - declares a state variable inside a function
	*/

	const getMembers = () => {
		return [
			{
				_id: "123abc",
				firstName: "Juan",
				lastName: "Dela Cruz",
				position: "Instructor",
				team: "Instructor Team"
			},
			{
				_id: "124abd",
				firstName: "Antonio",
				lastName: "Cruz",
				position: "HR Manager",
				team: "HR Team"
			},
			{
				_id: "125abe",
				firstName: "Emma",
				lastName: "Garcia",
				position: "Student",
				team: "N/A"
			},
			{
				_id: "126abf",
				firstName: "Adam",
				lastName: "Garcia",
				position: "Developer",
				team: "Tech Team"
			},
			{
				_id: "127abg",
				firstName: "Juan2",
				lastName: "Dela Cruz2",
				position: "Instructor",
				team: "Instructor Team"
			}
		]
		// this.setState({ members: membersCollection })
	}

	//mounted
	useEffect(() => {
		console.log("Component mounted: ")
		console.log(members)
		//set values to state
		setMembers(getMembers())
	}, [])


	useEffect(() => {
		console.log("Component updated: ")
		console.log(members)
	//update values to state
	}, [members])


		return (
				<Card>
					<Card.Header>
						<Card.Header.Title>
							Member List
						</Card.Header.Title>
					</Card.Header>
					<Card.Content>
						<table className="table is-fullwidth is-bordered is-striped is-hoverable">
							<thead>
								<tr>
									<th>Member Name</th>
									<th>Position</th>
									<th>Team</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
							{	//before
							/*
								this.state.members.map(member => {
									return <MemberRow x={member}/>
								})
							*/
								//after refactor
								members.map(member => {
									return <MemberRow x={member}/>
								})

							}
							</tbody>
						</table>
					</Card.Content>
				</Card>
			)
}

export default MemberList;