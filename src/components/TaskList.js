import React, { Component } from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import {
	Card
} from 'react-bulma-components';
import TaskRow from './TaskRow';

class TaskList extends Component {
	state ={
		tasks: []
	}

	componentDidMount()
	{
		let tasksCollection = [
			{
				_id: "taskid123a",
				desc: "task_one",
				assignedTeam: "HR Team",
				action: " "
			},
			{
				_id: "taskid123b",
				desc: "task_two",
				assignedTeam: "Sample Team",
				action: " "
			},
			{
				_id: "taskid123c",
				desc: "task_three",
				assignedTeam: "Sample Team",
				action: " "
			},
			{
				_id: "taskid123d",
				desc: "task_four",
				assignedTeam: "Sample Team",
				action: " "
			},
			{
				_id: "taskid123e",
				desc: "task_five",
				assignedTeam: "Sample Team",
				action: " "
			}
		]
	
		this.setState({ tasks: tasksCollection})

		console.log("Component mounted: ")
		console.log(this.state.tasks)

	}

	componentDidUpdate()
	{
		console.log("Component updated: ")
		console.log(this.state.tasks)
	}


	render()
	{
		return (
				<Card>
					<Card.Header>
						<Card.Header.Title>
							Task List
						</Card.Header.Title>
					</Card.Header>
					<Card.Content>
						<table className="table is-fullwidth is-bordered is-striped is-hoverable">
							<thead>
								<tr>
									<th>Task Description</th>
									<th>Assigned Team</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
							{
								this.state.tasks.map(task => {
									return <TaskRow x={task}/>
								})
							}
							</tbody>
						</table>
					</Card.Content>
				</Card>
			)
	}
}

export default TaskList;