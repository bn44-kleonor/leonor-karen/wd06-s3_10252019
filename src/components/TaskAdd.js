import React, { Component } from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import {
	Card
} from 'react-bulma-components';


class TaskAdd extends Component {


	state ={
		teams: []
	}

	componentDidMount()
	{
		let teamsCollection = [
			{
				_id: "teamid123a",
				name: "Instructor Team",
				action: " "
			},
			{
				_id: "teamid123b",
				name: "Tech Team",
				action: " "
			},
			{
				_id: "teamid123c",
				name: "HR Team",
				action: " "
			},
			{
				_id: "teamid123d",
				name: "N/A",
				action: " "
			},
			{
				_id: "teamid123e",
				name: "Others",
				action: " "
			}
		]


		this.setState({ teams: teamsCollection})

		console.log("Component mounted: ")
		console.log(this.state.teams)

	}

	componentDidUpdate()
	{
		console.log("Component updated: ")
		console.log(this.state.teams)
	}


	render()
	{
		return (
			<Card>
				<Card.Header>
					<Card.Header.Title>
						Add Task
					</Card.Header.Title>
				</Card.Header>
				<Card.Content>
					<form>
						<div className="field">
							<label className="label">Task Description</label>
							<div className="control">
								<input className="input" type="text" />
							</div>
						</div>
						<div className="field">
						  <label className="label">Team</label>
						  <div className="control">
						    <div className="select is-fullwidth">
						      <select>
						        {
									this.state.teams.map(team => {
										return <option key={team._id}>
													{team.name}
												</option>
									})
								}
						      </select>
						    </div>
						  </div>
						</div>
						<div className="field">
							<div className="control">
								<button className="button is-link is-success">Add</button>
							</div>
						</div>
					</form>
				</Card.Content>
			</Card>
		)
	}
}

export default TaskAdd;