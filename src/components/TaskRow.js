import React, { Component } from 'react';

const TaskRow = (props) => {

		const task = props.x;
		return (
			<tr key={task._id}>
				<td>{task.desc}</td>
				<td>{task.assignedTeam}</td>
				<td>{task.action}</td>
			</tr>
		)
}
export default TaskRow;