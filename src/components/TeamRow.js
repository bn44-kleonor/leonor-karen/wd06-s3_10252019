import React, { Component } from 'react';

const TeamRow = (props) => {

		const team = props.x;
		return (
				<tr key={team._id}>
					<td>{team.name}</td>
					<td>{team.action}</td>
				</tr>
		)
}

export default TeamRow;
