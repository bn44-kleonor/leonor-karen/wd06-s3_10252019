import React, { Component } from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import {
	Card
} from 'react-bulma-components';


class MemberAdd extends Component {
	render()
	{
		return (
				<Card>
					<Card.Header>
						<Card.Header.Title>
							Add Member
						</Card.Header.Title>
					</Card.Header>
					<Card.Content>
						<form>
							<div className="field">
							  <label className="label">First Name</label>
							  <div className="control">
							    <input className="input" type="text" />
							  </div>
							</div>
							<div className="field">
							  <label className="label">Last Name</label>
							  <div className="control">
							    <input className="input" type="text" />
							  </div>
							</div>
							<div className="field">
							  <label className="label">Position Name</label>
							  <div className="control">
							    <input className="input" type="text" />
							  </div>
							</div>
							<div className="field">
							  <label className="label">Team</label>
							  <div className="control">
							    <div className="select is-fullwidth">
							      <select>
							        <option>Select team</option>
							        <option>With options</option>
							      </select>
							    </div>
							  </div>
							</div>
							<br/>
							<div className="field">
							  <div className="control">
							    <button className="button is-link is-success">Add</button>
							  </div>
							</div>
						</form>
					</Card.Content>
				</Card>
			)
	}
}

export default MemberAdd;
