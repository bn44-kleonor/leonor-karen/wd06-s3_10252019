import React, { useState, useEffect } from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import {
	Card
} from 'react-bulma-components';
import TeamRow from './TeamRow';

const TeamList = (props) => {
	const [teams, setTeams] = useState([]);

	const getTeams = () => {
		return [
			{
				_id: "teamid123a",
				name: "Instructor Team",
				action: " "
			},
			{
				_id: "teamid123b",
				name: "Tech Team",
				action: " "
			},
			{
				_id: "teamid123c",
				name: "HR Team",
				action: " "
			},
			{
				_id: "teamid123d",
				name: "N/A",
				action: " "
			},
			{
				_id: "teamid123e",
				name: "Others",
				action: " "
			}
		]
	}

	//mounted
	useEffect(() => {
		console.log("Component mounted: ")
		console.log(teams)
		//set values to state
		setTeams(getTeams())
	}, [])


	useEffect(() => {
		console.log("Component updated: ")
		console.log(teams)
	//update values to state
	}, [teams])


		return (
				<Card>
					<Card.Header>
						<Card.Header.Title>
							Team List
						</Card.Header.Title>
					</Card.Header>
					<Card.Content>
						<table className="table is-fullwidth is-bordered is-striped is-hoverable">
							<thead>
								<tr>
									<th>Team Name</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
							{
								teams.map(team => {
									return <TeamRow x={team}/>
								})
							}
							</tbody>
						</table>
					</Card.Content>
				</Card>
			)

}

export default TeamList;