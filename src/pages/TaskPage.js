import React, { Component } from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import {
	Section,
	Heading,
	Columns
} from 'react-bulma-components';

import TaskAdd from '../components/TaskAdd';
import TaskList from '../components/TaskList';


class TaskPage extends Component {
	render()
	{
		const sectionStyle = {
			paddingTop: "3em",
			paddingBottom: '3em'
		};

		return (
			<Section size="medium" style={ sectionStyle }>
				<Heading>Tasks</Heading>
				<Columns>
					<Columns.Column size={4}>
						<TaskAdd />
					</Columns.Column>
					<Columns.Column>
						<TaskList />
					</Columns.Column>
				</Columns>
			</Section>
		)
	}
}

export default TaskPage;
