import React, { Component } from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import {
	Section,
	Heading,
	Columns
} from 'react-bulma-components';

import TeamAdd from '../components/TeamAdd';
import TeamList from '../components/TeamList';


class TeamPage extends Component {
	render()
	{
		const sectionStyle = {
			paddingTop: "3em",
			paddingBottom: '3em'
		};

		return (
			<Section size="medium" style={ sectionStyle }>
				<Heading>Teams</Heading>
				<Columns>
					<Columns.Column size={4}>
						<TeamAdd />
					</Columns.Column>
					<Columns.Column>
						<TeamList />
					</Columns.Column>
				</Columns>
			</Section>
		)
	}
}

export default TeamPage;
