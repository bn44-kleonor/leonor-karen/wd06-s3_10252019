import React from 'react';
import ReactDOM from 'react-dom';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import { 
	Navbar,
	Section,
	Heading,
	Columns
} from 'react-bulma-components';

const root = document.querySelector("#root");

const sectionStyle = {
	paddingTop: "3em",
	paddingBottom: '3em'
};

const pageComponent = (
	<React.Fragment>
		<Navbar className="is-black">
			<Navbar.Brand>
				<Navbar.Item>
					<strong>MERNG Tracker</strong>
				</Navbar.Item>
				<Navbar.Burger />
			</Navbar.Brand>
			<Navbar.Menu>
				<Navbar.Container>
					<Navbar.Item>Members</Navbar.Item>
					<Navbar.Item>Teams</Navbar.Item>
					<Navbar.Item>Tasks</Navbar.Item>
				</Navbar.Container>
			</Navbar.Menu>
		</Navbar>
		<Section size="medium" style={ sectionStyle }>
			<Heading>Members</Heading>
			<Columns>
				<Columns.Column class="is-4">
				  <div class="column">
					<div class="card">
					  <header class="card-header">
					    <p class="card-header-title">
					      Add Member
					    </p>
					  </header>
					  <div class="card-content">
						<div class="field">
						  <label class="label">First Name</label>
						  <div class="control">
						    <input class="input" type="text" placeholder="e.g Alex Smith" />
						  </div>
						</div>
						<div class="field">
						  <label class="label">Last Name</label>
						  <div class="control">
						    <input class="input" type="text" placeholder="e.g Alex Smith" />
						  </div>
						</div>
						<div class="field">
						  <label class="label">Position Name</label>
						  <div class="control">
						    <input class="input" type="text" placeholder="e.g Alex Smith" />
						  </div>
						</div>
						<div class="control">
						  <label class="label">Team</label>
						  <div class="select">
						    <select>
						      <option>Select a Team</option>
						      <option>With options</option>
						    </select>
						  </div>
						</div>
					  </div>
					  <div class="card-content">
					  	<button class="button is-primary">Add</button>
					  </div>
					</div>
				  </div>
				</Columns.Column>

				<Columns.Column>
				  <div class="column">
					<div class="card is-8">
					  <header class="card-header">
					    <p class="card-header-title">
					      Members List
					    </p>
					  </header>
					  <div class="card-content is-8">
						<div class="table-container">
						  <table class="table table is-bordered table is-fullwidth">
						  	<thead>
						  		<tr>
						  			<th>Member Name</th>
						  			<th>Position</th>
						  			<th>Team</th>
						  			<th>Action</th>
						  		</tr>
						  	</thead>
						  	<tbody>
						  		<tr>
						  			<td>Sample</td>
						  			<td>Sample</td>
						  			<td>Sample</td>
						  			<td>Sample</td>
						  		</tr>
						  	</tbody>
						  </table>
						</div>
					  </div>
					</div>
				  </div>
				</Columns.Column>
			</Columns>
		</Section>
	</React.Fragment>
);

ReactDOM.render(pageComponent, root);
